import admin from 'firebase-admin'
import { FirebaseUtils } from './FirebaseUtils'

describe('FirebaseUtil', () => {
  const appOptions = {
    credential: admin.credential.cert({
      projectId: 'winkies-1',
      privateKey:
        '-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCeH8X+gsO92rMe\nI0LFEZ3ehQt6Rmu5BHJQNRgCRCmCmM3A2NHdp0tLNqxjrvC7MsZja6f9nAdLt84U\nsVYKijjh3O6CezWDiMwnUCsLZyvmpsxymiTRHyCpJBqBE1frEWZpBqN75KEebVtP\nt5flkmNxA0Dt+9jQjAEIgaJ2qt5VdMM1iDKpeUaxSJegkUq/3Yq4P66QX+C+8OCg\nPKGiSYlt6eM5YIMKKMl1fBHggv3QXQVfSGKKdaVukAbGrb539dl26LNoG/Hz5k1j\nCY86jWMs5iQ33+vWp6NRf2XoCTKibe4oxqCTNBTJa44610WBLN25X50L7SJIgWT5\naW64/h+TAgMBAAECggEAQbdIzTbBbRUIXh0fcdGZf9Jha8b6nCE59RWrzb1QVyN0\nV5KE3g7aHv8w9BjPma1HgChM3/zJl3FjZ3wwGx9I9q2gPoBARFNFLY2G5i1RtIFb\n9MEVTggWxdToYv6MCnjny/xjU2k4gPHIAkB2E4YcZ+qHQKDrgfi9sTBXOeRXlMHT\n11OMUZwEp1flZcdmN4SEwoI5h1krmM92VXVT54KGXlK2IoLJhsfNYbiDLepygN5D\ngmtt1BVA4nn2VOYrpsy+eXtJoJ38KncrjOV/z9A/9ZOUszTYtbTf22KgxIkkOX/Y\nZolWmp+0T4h1kJ4aGEnr07ux5OQLEd/MZdtyW7wsmQKBgQDOulBdAeBP3PywQDWm\nrRh6BAM6TaHBJWxQABK+BO/OqTxIdNyNhalsin/aGOdyDp61cG5E9cn/f6B9YHD6\nKFYZEaoqSd5kKTCRRnKatQ6amkalIkwpWFgM1oFylUtz11ej84FdsJ25CreGgqEe\n3MhQJEIgyW8L6omAA4d385RptQKBgQDDz9yVTsr0cqxso1/mEHgyolTMEer1A1mL\nYbq5Vu3n1nqjlPBFxSZmbL03rBwVGeOkNQ22r3AiVzzTijFx/ruR5/B3d729O9Bm\nSfoUXL2eKCMNJHgsrYkHcb+MmahHv8xZWwwpQSNfM5dTIuI4g+22Ff+cCqT2QGQX\nxkwOvwkRJwKBgCsAFjdCbKYwEV+X+5Dv8Gdw7jryavMCFcWOPlfILxmNmjx4Emhd\npSAuruluxLW01M5o1IcDmUDMC1YI+XcPbNlKE1s4gknTR4bwYGZNwvvlm0w+FhmD\nhs0k0UhERa2S3Wf8/qu+URBsakBQa7M/uWryH88yFycjYfZ+xW7t+7EBAoGAV9wG\nn7/w9OoXT8+mkfZF2IJaBg7sG+/a192MCOlmaiYkdQ25GbUyW7bi1/4Tw6wQJnff\npV7ms6HtGwXg1cN0qi0mCpThv+z8v0jmOwi51CMyRw1cFr/uwjZZul6f0UHQES/3\nLvOZSXg+qAD4/F7F5HPOc1ebWRrfToOdUOiKyvUCgYBm1yVoqZZbKTySF9ffLNZK\nk/Vg/l6/v1znvzT6sjZQI2hHZEFE6GpJsfMSSuO/joO+0pvFdLQ8ZGLXu75U4K4z\nJG7Lr5ggLByK/zAJMuz/pIls1/KSlYVLHqMPctrhJzIwelt7Pa7lH8UAIuoqYfmf\n/jSBroR50lDfQnO71Nhtfg==\n-----END PRIVATE KEY-----\n'.replace(
          /\\n/g,
          '\n'
        ),
      clientEmail: 'winkies-1@appspot.gserviceaccount.com',
    }),
    databaseURL: 'https://winkies-1.firebaseio.com',
  }

  test('usersIterator() should iterate over 5 users with asyncIterable', async () => {
    const firebaseUtils = new FirebaseUtils(appOptions)

    let maxLoop = 5
    const users = []
    for await (const u of firebaseUtils.usersIterable()) {
      users.push(u)
      if (--maxLoop === 0) break
    }
    expect(users).toHaveLength(5)
  })

  test('userDeleteAll() should delete users by batches of 1000', async () => {
    const firebaseUtils = new FirebaseUtils(appOptions)
    await firebaseUtils.userDeleteAll()
  }, 999999999)
})
