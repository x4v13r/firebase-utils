import admin from 'firebase-admin'
import { AppOptions } from 'firebase-admin/lib/app'
import { ListUsersResult } from 'firebase-admin/lib/auth/base-auth'
import { UserRecord } from 'firebase-admin/lib/auth/user-record'

export class FirebaseUtils {
  // https://firebase.google.com/docs/admin/setup#initialize_the_sdk
  constructor(appOptions: AppOptions) {
    admin.initializeApp(appOptions)
  }

  userDelete(uid: string) {
    admin
      .auth()
      .deleteUser(uid)
      .then(function () {
        console.log('Successfully deleted user', uid)
      })
      .catch(function (error) {
        console.log('Error deleting user:', error)
      })
  }

  async userDeleteAll(nextPageToken?: string) {
    const listUsersResult = await admin.auth().listUsers(1000, nextPageToken)
    const uids = listUsersResult.users.map((u) => u.uid)
    const deleteUsersResult = await admin.auth().deleteUsers(uids)
    console.log(deleteUsersResult)

    if (listUsersResult.pageToken) {
      await this.userDeleteAll(listUsersResult.pageToken)
    }
  }

  async getAllUsers(nextPageToken?: string) {
    const self = this
    const listUsersResult = await admin.auth().listUsers(100, nextPageToken)
    listUsersResult.users.forEach((userRecord) => {
      // self.deleteUser(userRecord.uid)
      console.log(userRecord.uid)
    })
    if (listUsersResult.pageToken) {
      self.getAllUsers(listUsersResult.pageToken)
    }
  }

  usersIterable(): AsyncIterable<UserRecord> {
    return {
      [Symbol.asyncIterator]() {
        let node: ListUsersResult
        return {
          next: async () => {
            if (!node) node = await admin.auth().listUsers(1)
            if (!node?.users?.length) return { done: true, value: null }
            const value = node.users[0]
            node = await admin.auth().listUsers(1, node.pageToken)
            return {
              done: false,
              value,
            }
          },
        }
      },
    }
  }
}
