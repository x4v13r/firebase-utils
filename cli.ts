#!/usr/bin/env node
/* eslint-disable no-console */
import { readFileSync } from 'fs'
import { credential } from 'firebase-admin'
import { FirebaseUtils } from './FirebaseUtils'
import applicationDefault = credential.applicationDefault

function usage() {
  console.info(`Usage: fbutils [options] [command]
   
   Commands:
     user:delete [userId...]                                delete users of comma separated uid list.`
  )
}

if (!process.env.GOOGLE_APPLICATION_CREDENTIALS) {
  console.error(
    'GOOGLE_APPLICATION_CREDENTIALS must be set to service account json file.'
  )
}
if (process.argv.length !== 1) {
  console.error('Missing arguments')
  usage()
  process.exit(1)
}
const expectedFile: string = process.argv[2]
const actualFile: string = process.argv[3]
const expectedString: string = readFileSync(expectedFile, 'utf-8')
const actualString: string = readFileSync(actualFile, 'utf-8')
const expected: any = JSON.parse(expectedString)
const actual: any = JSON.parse(actualString)

const firebaseUtils = new FirebaseUtils({
  credential: applicationDefault(),
  databaseURL: `https://winkies-1.firebaseio.com`,
})
firebaseUtils.getAllUsers()
process.exit(1)
